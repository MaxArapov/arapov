import math
g_funcs = {}
def summ(a,b):
     return a+b
def sub(a,b):
     return b-a
def mul(a,b):
      return a*b
def div(a,b):
     return b/a
def factorial(var):
    res=1
    if float(var)<0 or float(var)-int(var)!= 0 :
        print("Не возможен для отрицательрных и дробных чисел.")
        return 0
    else:
        for i in range(1,int(var+1)):
            res=res*i
        return res
def sqrt(D):
    if int(D)>=0:
        return math.sqrt(D)
    else:
        return 'E'

def empty(var):
    if len(var) == 0:
        return True
    else:
        return False

def infixToPostfix(str):
    stack = []
    res = []
    str = str.replace(" ", "")
    end = False
    clear = False
    i = 0
    buf = ['','','']
    temp = ""
    while not end:
        c = str[i]
        buf[0]=str[i-2]
        buf[1]=str[i-1]
        buf[2]=str[i]
        x = None
        flag_num = 0
        while c >= '0' and c <='9' or c=='.':
            temp+=c
            i = i + 1
            c = str[i]
            flag_num=1
        if flag_num==1:
            if buf[0]=='(' and buf[1]=='-' and buf[2]>='0' and buf[2]<='9':
                x=float(temp)*(-1)
                res.append(x)
                flag_num = 0
                temp = ""
                stack.pop()
                clear = True
            else:
                x = float(temp)
                res.append(x)
                flag_num=0
                temp=""
        if c == 'E':
            res.append(c)
        if c == ')' and buf != '' and clear==False:
            res.append(stack.pop())
        if c in ['+','-','/','*','!','$']:
            stack.append(c)
        i += 1
        if i >= len(str):
            end = True
        clear = empty(stack)
    #print(res) #<---Вывод выражения в обратной польской нотации
    return res

def calcExpression(str):
    stack = []
    str = infixToPostfix(str)
    funcs = {'+':summ,'-':sub,'*':mul,'/':div, '!':factorial, '$':sqrt}
    i = 0
    x = 0
    Error = False
    end = False
    while not end:
        c = str[i]
        if c == 'E':
            Error = True
            break
        x = 0
        if c in ['+','-','/','*','!','$']:
            if c in ['!','$']:
                a = stack.pop()
                x = funcs[c](a)
            else:
                a = stack.pop()
                b = stack.pop()
                x = funcs[c](a,b)
        else:
            x = float(c)
        stack.append(x)
        i = i + 1
        if i >= len(str):
            end = True
    if Error == False:
        return x
    else:
        return 0