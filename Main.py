import Calc

StackVariable = []


class Variable:
    def __init__(self, name, value):
        self.Value = value
        self.Name = name
        self.Index = 0


def showStackVariable():
    for i in StackVariable:
        print(i.Name, '=', i.Value)

path = input('') #<----Ввести имя файла QudEq.txt(квадратное уравнение) / Code.txr(факториал и сложение чисел)
file = open(path, mode='r')
#Цикл обработки переменных и выражений с их инициализацией
for line in file.readlines():
    Line = line.strip()
    ListToken = Line.split(' ')

    if (ListToken[0] == 'var') and (ListToken[2] == '='):
        if (ListToken[3] >= '0') and (ListToken[3] <= '9') or (ListToken[3] == '.') or (ListToken[3] >= '-0') and (ListToken[3] <= '-9'):
            StackVariable.append(Variable(ListToken[1], float(ListToken[3])))

    if len(ListToken) > 1:
        if ListToken[1] == '=':
            i = 0
            while i != len(ListToken[2]):
                for var in range(len(StackVariable)):
                    if i < len(ListToken[2]):
                        if ListToken[2][i] == StackVariable[var].Name:
                            temp_str=str(StackVariable[var].Value)
                            if temp_str[0]=='-':
                                if ListToken[2][i-1] == '-':
                                    if ListToken[2][i-2]=='(':
                                        temp_str = temp_str[1:]
                                        ListToken[2] = ListToken[2][0:i - 1] + temp_str + ListToken[2][i + 1:]
                                    else:
                                        temp_str=temp_str[1:]
                                        ListToken[2] = ListToken[2][0:i-1] + '+' +temp_str + ListToken[2][i + 1:]
                                else:
                                    if ListToken[2][i-1] not in ['(','*','+','-','/','!','$',')']:
                                        ListToken[2] = ListToken[2][0:i-1] + temp_str + ListToken[2][i+1:]
                                    else:
                                        ListToken[2] = ListToken[2][0:i] + '('+ temp_str +')'+ ListToken[2][i + 1:]
                            else:
                                ListToken[2] = ListToken[2][0:i] + temp_str + ListToken[2][i + 1:]
                            var = 0
                i += 1
            #print(ListToken[2]) #<---- Вывод выражения с учетом отрицательных чисел
            for var in range(len(StackVariable)):
                if ListToken[0] == StackVariable[var].Name:
                    StackVariable[var].Value=Calc.calcExpression(ListToken[2])



    if ListToken[0][0:6] == 'print(':
        for i in StackVariable:
            if i.Name == ListToken[0][6:-1]:
                stroka = str(i.Name + ' = ' + str(i.Value))
                print(stroka)


file.close()

# showStackVariable()
